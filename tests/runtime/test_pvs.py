import re

import pytest

from ..utils import run_ioc_get_output


@pytest.mark.parametrize(
    "pv, expected",
    [
        ("Modules", r"\"require\" *\"{name}\""),
        ("Versions", r"\"[0-9a-z.+-]+\" *\"{version}\""),
        ("ModuleVersions", r"require [0-9a-z.+-]+\\n{name} {version}\\n"),
        ("{name}Version", r"{version}"),
        ("requireVersion", r"[0-9a-z.+-]+"),
    ],
    ids=[
        "Modules",
        "Versions",
        "ModuleVersions",
        "modulenameVersion",
        "requireVersion",
    ],
)
def test_require_pvs(wrapper, pv, expected):
    cell_path = wrapper.wrapper_dir / "cellMods"
    _ = wrapper.run_make(
        "cellinstall",
        cell_path=cell_path,
    )
    _, stdout, _ = run_ioc_get_output(
        "-l",
        cell_path,
        "-r",
        wrapper.name,
        "-c",
        f"afterInit 'dbgf TEST:{pv.format(name=wrapper.name)}'",
        "--iocname",
        "TEST",
    )

    match = re.search(
        expected.format(
            name=wrapper.name,
            version=wrapper.version.replace(".", "\\.").replace("+", "\\+"),
        ),
        stdout,
    )
    assert match
