import re

import pytest

from ..utils import run_ioc_get_output

RE_MODULE_LOADED = "Loaded {module} version {version}"
RE_MODULE_NOT_LOADED = "Module {module} (not available|version {required} not available|version {required} not available \\(but other versions are available\\))"


@pytest.mark.parametrize(
    "requested, expected, installed",
    [
        # If nothing is installed, nothing should be loaded
        ("", "", []),
        # Test versions can be loaded
        ("test", "test", ["test", "0.0.1+0"]),
        # Numeric versions should be prioritized over test versions
        ("", "0.0.1+0", ["test", "0.0.1+0"]),
        ("", "0.0.1+0", ["0.0.1+0", "test"]),
        # Highest revision number should be loaded if version is unspecified
        ("", "0.0.1+7", ["0.0.1+0", "0.0.1+7", "0.0.1+3"]),
        # Only load the given revision number if it is specified
        ("0.0.1+0", "", ["0.0.1+1"]),
        # If no revision number is specified, load the highest revision number
        ("0.0.1", "0.0.1+4", ["0.1.0+0", "0.0.1+4", "0.0.1+0"]),
        # Revision number 0 means load that version exactly
        ("0.0.1+0", "0.0.1+0", ["0.0.1+0"]),
        ("0.0.1+0", "0.0.1+0", ["0.0.1+0", "0.0.1+1", "1.0.0+0"]),
        # 1-test counts as a test version, as does 1.0
        ("", "0.0.1+0", ["0.0.1+0", "1-test"]),
        ("", "0.0.1+0", ["0.0.1+0", "1.0"]),
        # Numeric version should be prioritised over "higher" test version
        ("", "0.1.0+0", ["0.1.0+0", "1.0.0-rc1"]),
        # Pick the latest test version based on revision number
        ("test", "test+2", ["test+0", "test+2"]),
        # Don't install an incorrect test version
        ("foo", "", ["bar"]),
        # Mix of test and numeric
        ("1.2.3-test", "1.2.3-test+2", ["1.2.3-test", "1.2.3-test+0", "1.2.3-test+2"]),
    ],
)
def test_require_versions(wrapper, requested, expected, installed):
    for version in installed:
        _ = wrapper.run_make(
            "clean",
            "cellinstall",
            module_version=version,
        )

    rc, stdout, stderr = run_ioc_get_output(
        module=wrapper.name,
        version=requested,
        cell_path=wrapper.wrapper_dir / "cellMods",
    )
    if expected:
        match = re.search(
            RE_MODULE_LOADED.format(module=wrapper.name, version=re.escape(expected)),
            stdout,
        )
        assert rc == 0
        assert match
    else:
        match = re.search(
            RE_MODULE_NOT_LOADED.format(
                module=wrapper.name, required=re.escape(requested)
            ),
            stderr,
        )
        assert rc != 0
        assert match


def test_afterinit():
    rc, outs, _ = run_ioc_get_output("-c", "afterInit 'echo hello'")
    assert rc == 0

    lines = outs.split("\n")
    assert "hello" in lines[lines.index("iocInit") :]


def test_afterinit_does_not_take_varargs():
    rc, outs, _ = run_ioc_get_output("-c", "afterInit should-run should-not-run")
    assert rc == 0

    lines = outs.split("\n")
    assert "should-not-run" not in lines[lines.index("iocInit") :]
