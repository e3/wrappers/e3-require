import os
import subprocess
from pathlib import Path
from random import choice
from string import ascii_lowercase
from typing import Tuple

import pytest


@pytest.fixture()
def wrapper_factory(tmp_path):
    class WrapperFactory:
        def create(self, **kwargs):
            return Wrapper(tmp_path, **kwargs)

    return WrapperFactory()


@pytest.fixture()
def wrapper(wrapper_factory):
    return wrapper_factory.create()


@pytest.fixture()
def module(tmp_path):
    return Module(tmp_path)


class Module:
    def __init__(self, tmp_test_dir: Path):
        self.name = "module-" + "".join(choice(ascii_lowercase) for _ in range(16))
        self.module_dir = tmp_test_dir / self.name
        self.module_dir.mkdir()

        # set up fake repo
        fake_file = self.module_dir / "random-file-to-commit"
        fake_file.write_text("whatever")
        for cmd in [
            "git init",
            "git add .",
            "git commit -m message -q",
            "git tag fake-tag",
        ]:
            subprocess.run(
                cmd.split(),
                cwd=self.module_dir,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )


class Wrapper:
    def __init__(
        self,
        tmp_test_dir: Path,
        *,
        name: str = None,
        **kwargs,
    ):
        epics_base = Path(os.environ["EPICS_BASE"])

        # environment
        self.base_version = epics_base.name.split("base-")[-1]
        self.host_arch = os.environ["EPICS_HOST_ARCH"]
        self.require_version = os.environ["E3_REQUIRE_VERSION"]

        # attributes
        self.name = name or "test_mod_" + "".join(
            choice(ascii_lowercase) for _ in range(16)
        )
        self.version = "0.0.0+0"

        e3_require_config = epics_base / "require" / self.require_version / "configure"
        module_path = kwargs.get("E3_MODULE_SRC_PATH", self.name)

        # source
        self.wrapper_dir = tmp_test_dir / f"e3-{self.name}"
        self.config_dir = self.wrapper_dir / "configure"
        self.module_dir = self.wrapper_dir / module_path
        self.config_module = self.config_dir / "CONFIG_MODULE"
        self.makefile = self.wrapper_dir / "Makefile"
        self.module_makefile = self.wrapper_dir / f"{self.name}.Makefile"

        # build
        self.build_dir = self.module_dir / f"O.{self.base_version}_{self.host_arch}"

        # install
        self.install_dir = (
            self.wrapper_dir
            / "cellMods"
            / f"base-{self.base_version}"
            / f"require-{self.require_version}"
        )
        self.package_dir = self.install_dir / self.name / self.version

        # set up files
        self.module_dir.mkdir(parents=True)
        self.config_dir.mkdir()
        self.config_module.touch()

        self._add_root_makefile(module_path, e3_require_config)
        self._add_module_makefile()
        self.add_file("test.dbd")

    def _add_root_makefile(self, module_path, e3_require_config) -> None:
        makefile_contents = f"""
TOP:=$(CURDIR)

E3_MODULE_NAME:={self.name}
E3_MODULE_VERSION:={self.version}
E3_MODULE_SRC_PATH:={module_path}
E3_MODULE_MAKEFILE:={self.name}.Makefile

include $(TOP)/configure/CONFIG_MODULE
-include $(TOP)/configure/CONFIG_MODULE.local

REQUIRE_CONFIG:={e3_require_config}

include $(REQUIRE_CONFIG)/CONFIG
include $(REQUIRE_CONFIG)/RULES_SITEMODS
"""
        self.makefile.write_text(makefile_contents)

    def _add_module_makefile(self) -> None:
        module_makefile_contents = """
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
EXCLUDE_ARCHS+=debug
"""
        self.module_makefile.write_text(module_makefile_contents)

    def add_file(self, name):
        (self.module_dir / name).touch()

    def add_directory(self, name):
        (self.module_dir / name).mkdir()

    def write_var_to_makefile(
        self,
        file: Path,
        makefile_var: str,
        value: str,
        modifier: str = None,
        mode: str = "a",
        export: bool = False,
    ):
        with open(file, mode) as f:
            f.write(
                f"{'export' if export else ''} {makefile_var} {modifier if modifier else ''}= {value}\n"
            )

    def run_make(
        self,
        target: str,
        *args,
        module_version: str = None,
        cell_path: str = None,
        **kwargs,
    ) -> Tuple[int, bytes, bytes]:
        """Attempt to run `make <target> <args>` for the current wrapper."""

        # We should not install in the global environment during tests
        # so we use the cell commands
        if target in ["install", "uninstall"]:
            target = "cell" + target

        args = list(args)
        if module_version:
            args.append(f"E3_MODULE_VERSION={module_version}")

        # These are added as arguments to make to ensure that they are prioritised
        for k, v in kwargs.items():
            args.append(f"{k}={v}")

        if cell_path:
            # this is often/usually a Path object, so let's cast it for sanity of mind
            cell_path = str(cell_path)
            self.write_var_to_makefile(
                self.config_dir / "CONFIG_CELL.local", "E3_CELL_PATH", cell_path
            )
        make_cmd = ["make", "-C", self.wrapper_dir, target, *args]
        p = subprocess.Popen(
            make_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            env=os.environ.copy(),
        )
        outs, errs = p.communicate()
        return p.returncode, outs, errs
