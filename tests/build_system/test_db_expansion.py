def test_automated_subs_tmps_db_expansion(wrapper):
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "TMPS", "templates/a.template", modifier="+"
    )
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "SUBS", "b.substitutions", modifier="+"
    )
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "USR_DBFLAGS", "-I templates", modifier="+"
    )

    template_dir = wrapper.module_dir / "templates"
    template_dir.mkdir()
    template_file = template_dir / "a.template"
    template_file_contents = "record (ai, $(P)) {}"
    template_file.write_text(template_file_contents)

    substitution_file = wrapper.module_dir / "b.substitutions"
    substitution_file.write_text(
        """file "a.template"
{
  pattern { P }
          { "$(PREF)" }
}
"""
    )

    common_dir = wrapper.module_dir / f"O.{wrapper.base_version}_Common"

    rc, *_ = wrapper.run_make("db_internal")
    assert rc == 0

    expanded_template_file = common_dir / "a.db"
    assert expanded_template_file.read_text() == template_file_contents

    expanded_substitution_file = common_dir / "b.db"
    assert expanded_substitution_file.read_text() == template_file_contents.replace(
        "$(P)", "$(PREF)"
    )


def test_automated_subs_tmps_db_expansion_priority(wrapper):
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "TMPS", "a.template", modifier="+"
    )
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "SUBS", "a.substitutions", modifier="+"
    )
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "USR_DBFLAGS", "-I.", modifier="+"
    )

    template_file = wrapper.module_dir / "a.template"
    template_file_contents = "record (ai, $(FOO)) {}"
    template_file.write_text(template_file_contents)

    substitution_file = wrapper.module_dir / "a.substitutions"
    substitution_file.write_text(
        """file "a.template"
{
  pattern { FOO }
          { "$(BAR)" }
}
"""
    )

    common_dir = wrapper.module_dir / f"O.{wrapper.base_version}_Common"

    rc, *_ = wrapper.run_make("db_internal")
    assert rc == 0

    expanded_template_file = common_dir / "a.db"
    assert "BAR" in expanded_template_file.read_text()
    assert "FOO" not in expanded_template_file.read_text()
