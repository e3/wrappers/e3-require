import pytest


@pytest.mark.parametrize(
    "name",
    [
        "lowercase",
        "under_score",
        "numb3r",
    ],
)
def test_no_error_if_valid_characters_in_module_name(wrapper_factory, name):
    wrapper = wrapper_factory.create(name=name)

    rc, *_ = wrapper.run_make("build")
    assert rc == 0


@pytest.mark.parametrize(
    "name",
    [
        "Capitalised",
        "ALLCAPS",
        "hyp-hen",
        "symbol?",
    ],
)
def test_error_if_invalid_characters_in_module_name(wrapper_factory, name):
    wrapper = wrapper_factory.create(name=name)

    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert f"E3_MODULE_NAME '{name}' is not valid" in errs
