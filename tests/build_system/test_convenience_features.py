def test_append_missing_revision_number(wrapper):
    version = "1.2.3"
    wrapper.write_var_to_makefile(
        wrapper.config_dir / "CONFIG_MODULE.local", "E3_MODULE_VERSION", version
    )

    rc, outs, _ = wrapper.run_make("vars")
    assert rc == 0
    assert f"E3_MODULE_VERSION = {version}+0" in outs.splitlines()


def test_warning_msg_if_building_from_sitemods(wrapper):
    rc, _, errs = wrapper.run_make(
        "build_path_check", E3_MODULES_PATH=wrapper.wrapper_dir / ".."
    )
    assert rc == 0
    assert (
        "It is ill-advised to build a module from the module install location." in errs
    )
