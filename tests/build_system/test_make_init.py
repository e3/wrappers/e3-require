import shutil
import subprocess

import pytest


def set_up_git_submodule(main, sub) -> None:
    # remove local dir with dbd-file and add fake repo as submodule
    shutil.rmtree(main.module_dir)
    subprocess.run(
        ["git", "init"],
        cwd=main.wrapper_dir,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    # we allow file:// as protocol in order to not get a fatal error like
    #   fatal: transport 'file' not allowed
    # see https://lore.kernel.org/git/000001d91c8b$6a26cd60$3e746820$@nexbridge.com/T/
    subprocess.run(
        [
            "git",
            "-c",
            "protocol.file.allow=always",
            "submodule",
            "add",
            f"file://{sub.module_dir.as_posix()}",
            main.name,
        ],
        cwd=main.wrapper_dir,
    )
    main.write_var_to_makefile(
        main.config_module,
        "EPICS_MODULE_TAG",
        "fake-tag",
    )


def test_error_if_epics_module_tag_set_but_no_submodule(wrapper):
    wrapper.write_var_to_makefile(
        wrapper.config_module,
        "EPICS_MODULE_TAG",
        "whatever",
    )
    rc, _, stderr = wrapper.run_make("init")
    assert rc != 0
    assert stderr


@pytest.mark.skipif(
    subprocess.run(
        ["git", "config", "get", "protocol.file.allow"], stdout=subprocess.PIPE
    )
    .stdout.decode()
    .strip()
    != "always",
    reason="file protocol must be allowed",
)
def test_init_inits_without_error_if_defined(wrapper, module):
    set_up_git_submodule(wrapper, module)

    rc, *_ = wrapper.run_make("init")
    assert rc == 0


def test_init_fetches_submodule_files(wrapper, module):
    set_up_git_submodule(wrapper, module)

    _ = wrapper.run_make("init")
    assert len(list((wrapper.wrapper_dir / wrapper.name).iterdir())) > 0


def test_init_checks_out_version_in_epics_module_tag(wrapper, module):
    set_up_git_submodule(wrapper, module)

    _ = wrapper.run_make("init")
    res = subprocess.run(
        ["git", "describe", "--tags"], cwd=wrapper.module_dir, stdout=subprocess.PIPE
    )
    assert res.stdout.decode().strip() == "fake-tag"


def test_info_msg_if_local_mode(wrapper):
    rc, outs, _ = wrapper.run_make("init")
    assert rc == 0
    assert "You are in the local source mode" in outs
