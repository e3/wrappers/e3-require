from pathlib import Path

import pytest


def create_patch_file(path: Path, filename: str, desc: str):
    path.parent.mkdir(parents=True, exist_ok=True)
    patch_file_contents = """
diff --git {filename} {filename}
index 1806ff6..8701832 100644
--- {filename}
+++ {filename}
@@ -1,3 +1,3 @@
 record(ai, "TEST") {{
-
+    field(DESC, "{desc}")
 }}
"""
    path.write_text(patch_file_contents.format(filename=filename, desc=desc))


def test_patch_target_applies_patch_in_patch_dir(wrapper):
    db_path = wrapper.module_dir / "database.db"
    db_file_contents = """record(ai, "TEST") {

}
"""
    db_path.write_text(db_file_contents)
    patch_dir = wrapper.wrapper_dir / "patch"
    create_patch_file(patch_dir / "apply.p0.patch", "database.db", "OK")

    rc, *_ = wrapper.run_make("patch")
    assert rc == 0
    assert 'field(DESC, "OK")' in db_path.read_text()

    rc, *_ = wrapper.run_make("build")
    assert rc == 0
    assert any((wrapper.module_dir).glob("O.*"))

    rc, *_ = wrapper.run_make("cellinstall")
    assert rc == 0
    assert any((wrapper.wrapper_dir / "cellMods").glob("**/*.db"))


def test_patch_target_does_not_apply_patch_outside_patch_dir(wrapper):
    db_path = wrapper.module_dir / "database.db"
    db_file_contents = """record(ai, "TEST") {

}
"""
    db_path.write_text(db_file_contents)
    patch_dir = wrapper.wrapper_dir / "patch"
    create_patch_file(
        patch_dir / wrapper.version / "do-not-apply.p0.patch", "database.db", "NOK"
    )

    rc, *_ = wrapper.run_make("patch")
    assert rc == 0
    assert "NOK" not in db_path.read_text()


@pytest.mark.parametrize(
    "good_patch_first", [True, False], ids=["good-patch-first", "bad-patch-first"]
)
def test_error_if_patch_failure(wrapper, good_patch_first):
    db_path = wrapper.module_dir / "database.db"
    db_file_contents = """record(ai, "TEST") {

}
"""
    db_path.write_text(db_file_contents)
    patch_dir = wrapper.wrapper_dir / "patch"
    good_patch_order, bad_patch_order = (1, 2) if good_patch_first else (2, 1)
    create_patch_file(
        patch_dir / f"{good_patch_order}_good.p0.patch", "database.db", "OK"
    )
    create_patch_file(
        patch_dir / f"{bad_patch_order}_bad.p0.patch", "not_a_file", "this_should_fail"
    )

    rc, _, errs = wrapper.run_make("patch")
    assert rc == 2
    assert "not_a_file: No such file or directory" in errs
