import pytest


@pytest.mark.parametrize(
    "targets",
    [
        ["cellbuild", "build"],
        ["cellinstall", "install"],
        ["vars", "cellbuild"],
        ["debug", "cellinstall"],
        ["cellbuild", "uninstall"],
    ],
    ids=["build", "install", "vars", "debug", "uninstall"],
)
def test_error_if_celltarget_with_non_celltarget(wrapper, targets):
    rc, _, errs = wrapper.run_make(targets[0], targets[1])
    assert rc != 0
    assert (
        "cell targets cannot be called with non cell targets on the same command line"
        in errs
    )
