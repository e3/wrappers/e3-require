from pathlib import Path
import re
import sys

import pytest


def test_header_install_location(wrapper):
    subdir = wrapper.module_dir / "db" / "subdir"
    subdir.mkdir(parents=True)

    extensions = ["h", "hpp", "hxx", "hh"]
    for ext in extensions:
        wrapper.write_var_to_makefile(
            wrapper.module_makefile, "HEADERS", f"db/subdir/header.{ext}", modifier="+"
        )
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "KEEP_HEADER_SUBDIRS", "db", modifier="+"
    )
    for ext in extensions:
        (subdir / f"header.{ext}").touch()

    rc, *_ = wrapper.run_make("cellinstall")
    assert rc == 0
    cell_path = wrapper.package_dir
    for ext in extensions:
        assert (Path(cell_path) / "include" / "subdir" / f"header.{ext}").is_file()
        assert not (Path(cell_path) / "include" / f"header.{ext}").is_file()


@pytest.mark.parametrize("module_version", ["0.0.0+0", "test"])
def test_warning_msg_if_double_install(wrapper, module_version):
    RE_ERR_MOD_VER_EXISTS = (
        "Skipping installation of.*{module}/{version}.*it is already installed"
    )
    rc, *_ = wrapper.run_make("install", module_version=module_version)

    rc, _, errs = wrapper.run_make("install", module_version=module_version)
    assert rc == 0
    assert re.search(
        RE_ERR_MOD_VER_EXISTS.format(
            module=re.escape(wrapper.name), version=re.escape(module_version)
        ),
        errs,
    )


@pytest.mark.skipif(
    sys.platform != "linux", reason="only have cross-compilers for linux"
)
def test_install_missing_archs(wrapper):
    RE_SKIP_ARCH = "Skipping installation of.*{module}/{version}/lib/{arch} because it is already installed"

    cross_arch = "foo"
    # Some explaining is needed here. In order to "add an architecture" we need
    # to define a lot of configuration that comes with EPICS base. This is usually
    # done in driver.makefile when we load ${EPICS_BASE}/CONFIG... in the middle
    # of driver.makefile. This is quite difficult to mock, since there is not a
    # good way to insert a new config file either at that time or place.

    # The following does, however, work: we add a new arch (foo) and we simply
    # tell driver.makefile that it should "act like" the host arch.
    wrapper.module_makefile.write_text(
        f"""
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

_tmpmkfile := $(lastword $(MAKEFILE_LIST))

ifeq ($(T_A),{cross_arch})
CONFIG:=$(EPICS_BASE)/configure
include $(CONFIG)/os/CONFIG.Common.{wrapper.host_arch}
endif
MAKEFILE_LIST := $(MAKEFILE_LIST) $(_tmpmkfile)

include $(E3_REQUIRE_TOOLS)/driver.makefile

CROSS_COMPILER_TARGET_ARCHS += {cross_arch}
"""
    )
    rc, *_ = wrapper.run_make("install", EXCLUDE_ARCHS=cross_arch)
    assert rc == 0
    assert (wrapper.package_dir / "lib" / wrapper.host_arch).is_dir()
    assert not (wrapper.package_dir / "lib" / cross_arch).exists()

    rc, _, errs = wrapper.run_make("install")
    assert rc == 0
    assert re.search(
        RE_SKIP_ARCH.format(
            module=re.escape(wrapper.name),
            version=re.escape(wrapper.version),
            arch=re.escape(wrapper.host_arch),
        ),
        errs,
    )
    assert not re.search(
        RE_SKIP_ARCH.format(
            module=re.escape(wrapper.name),
            version=re.escape(wrapper.version),
            arch=re.escape(cross_arch),
        ),
        errs,
    )
    assert (wrapper.package_dir / "lib" / cross_arch).is_dir()


def test_metadata_installed_on_success(wrapper):
    rc, *_ = wrapper.run_make("install")
    assert rc == 0
    assert (wrapper.package_dir / f"{wrapper.name}_meta.yaml").exists()


def test_metadata_not_installed_if_arch_fails(wrapper):
    cross_arch = "foo"
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, f"VLIBS_{cross_arch}", "not_a_library", modifier="+"
    )

    rc, *_ = wrapper.run_make(
        "install", CROSS_COMPILER_TARGET_ARCHS=cross_arch, OS_CLASS="Linux"
    )
    assert rc == 2
    assert not (wrapper.package_dir / f"{wrapper.name}_meta.yaml").exists()


def test_licenses_are_installed(wrapper):
    wrapper.add_file("LICENSE")
    wrapper.add_directory("foo")
    wrapper.add_file("foo/LICENSE")

    rc, out, _ = wrapper.run_make("install")
    assert rc == 0
    assert re.search("Installing license file .././LICENSE", out)
    assert re.search("Installing license file .././foo/LICENSE", out)

    file_path = wrapper.package_dir / "doc/LICENSE"
    assert file_path.exists()

    file_path = wrapper.package_dir / "doc/foo/LICENSE"
    assert file_path.exists()
