import sys

import pytest

from ..utils import run_ioc_get_output


def test_match_versions(wrapper_factory):
    # This tests if the dependencies
    #    B->A->C
    #    B->C
    # with A and B both requesting same version of C

    wrapper_dep = wrapper_factory.create()
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()

    cell_path = wrapper_b.wrapper_dir / "cellmods"

    dep_version = "1.0.0+0"

    a_version = "0.0.0+0"
    b_version = "0.0.0+0"

    # Wrapper a dependes on dep,1.0.0+0
    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        dep_version,
        export=True,
    )

    # Wrapper b depends on dep,1.0.0+0
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        dep_version,
        export=True,
    )

    # Wrapper b also depends on a
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module,
        f"{wrapper_a.name}_DEP_VERSION",
        a_version,
        export=True,
    )

    rc, *_ = wrapper_dep.run_make(
        "cellinstall", module_version=dep_version, cell_path=cell_path
    )
    assert rc == 0

    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=a_version, cell_path=cell_path
    )
    assert rc == 0

    # As wrappers a and b both depends on dep,1.0.0+0 this build should finish
    # corretly.
    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=b_version, cell_path=cell_path
    )
    assert rc == 0


def test_unmatching_versions(wrapper_factory):
    # This tests if the dependencies
    #    B->A->C
    #    B->C
    # but A depends on a version of C different than B

    wrapper_dep = wrapper_factory.create()
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()

    cell_path = wrapper_b.wrapper_dir / "cellmods"

    dep_version_1 = "1.0.0+0"
    dep_version_2 = "2.0.0+0"

    a_version = "0.0.0+0"
    b_version = "0.0.0+0"

    # Wrapper a dependes on dep v1
    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        dep_version_1,
        export=True,
    )

    # Wrapper b depends on dep v2
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        dep_version_2,
        export=True,
    )

    # Wrapper b also depends on wrapper_a
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module, f"{wrapper_a.name}_DEP_VERSION", a_version, export=True
    )

    rc, *_ = wrapper_dep.run_make(
        "cellinstall", module_version=dep_version_1, cell_path=cell_path
    )
    assert rc == 0

    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=a_version, cell_path=cell_path
    )
    assert rc == 0

    # Now a second installation of wrapper_dep but with version 2
    rc, *_ = wrapper_dep.run_make(
        "cellinstall", module_version=dep_version_2, cell_path=cell_path
    )
    assert rc == 0

    # This next installation should fail because B depends on A
    # that depends on DEP. However A needs DEP 1.0.0+0 and B
    # needs DEP 2.0.0+0
    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=b_version, cell_path=cell_path
    )
    assert rc != 0


def test_indirect_unmatching_versions(wrapper_factory):
    # This tests if the dependencies
    #    B->A->C
    #    B->D->C
    # but A depends on a version of C different than D

    wrapper_c = wrapper_factory.create()
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()
    wrapper_d = wrapper_factory.create()

    cell_path = wrapper_b.wrapper_dir / "cellmods"

    c_version_a = "1.0.0+0"
    c_version_d = "2.0.0+0"

    a_version = "0.0.0+0"
    d_version = "0.0.0+0"
    b_version = "0.0.0+0"

    # Wrapper a dependes on c
    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_c.name}_DEP_VERSION",
        c_version_a,
        export=True,
    )

    # Wrapper d dependes on c
    wrapper_d.write_var_to_makefile(
        wrapper_d.config_module,
        f"{wrapper_c.name}_DEP_VERSION",
        c_version_d,
        export=True,
    )

    # Wrapper b depends on d
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module, f"{wrapper_d.name}_DEP_VERSION", d_version, export=True
    )

    # Wrapper b also depends on a
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module, f"{wrapper_a.name}_DEP_VERSION", a_version, export=True
    )

    rc, *_ = wrapper_c.run_make(
        "cellinstall", module_version=c_version_a, cell_path=cell_path
    )
    assert rc == 0

    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=a_version, cell_path=cell_path
    )
    assert rc == 0

    # Now a second installation of wrapper_dep but with version 2
    rc, *_ = wrapper_c.run_make(
        "cellinstall", module_version=c_version_d, cell_path=cell_path
    )
    assert rc == 0

    rc, *_ = wrapper_d.run_make(
        "cellinstall", module_version=d_version, cell_path=cell_path
    )
    assert rc == 0

    # This next installation should fail because A depends on C
    # with a different version that D depends on C.
    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=b_version, cell_path=cell_path
    )
    assert rc != 0


def test_recursive_header_include(wrapper_factory):
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()
    wrapper_c = wrapper_factory.create()
    wrapper_d = wrapper_factory.create()

    cell_path = wrapper_a.wrapper_dir / "cellMods"

    module_version = "0.0.0+0"

    wrapper_c.write_var_to_makefile(
        wrapper_c.config_module,
        f"{wrapper_d.name}_DEP_VERSION",
        module_version,
        export=True,
    )
    wrapper_b.write_var_to_makefile(
        wrapper_b.config_module,
        f"{wrapper_c.name}_DEP_VERSION",
        module_version,
        export=True,
    )
    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_b.name}_DEP_VERSION",
        module_version,
        export=True,
    )

    wrapper_d.write_var_to_makefile(
        wrapper_d.module_makefile, "HEADERS", f"{wrapper_d.name}.h", modifier="+"
    )
    (wrapper_d.module_dir / f"{wrapper_d.name}.h").touch()

    wrapper_a.write_var_to_makefile(
        wrapper_a.module_makefile, "SOURCES", f"{wrapper_a.name}.c", modifier="+"
    )
    wrapper_a_source = wrapper_a.module_dir / f"{wrapper_a.name}.c"
    wrapper_a_source.write_text(f'#include "{wrapper_d.name}.h"')

    rc, *_ = wrapper_d.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_c.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0

    rc, outs, _ = run_ioc_get_output(
        module=wrapper_a.name,
        version=module_version,
        cell_path=wrapper_a.wrapper_dir / "cellMods",
    )
    assert rc == 0
    assert f"Loaded {wrapper_c.name} version {module_version}" in outs


def test_updated_dependencies(wrapper_factory):
    wrapper_dep = wrapper_factory.create()
    wrapper_main = wrapper_factory.create()

    cell_path = wrapper_main.wrapper_dir / "cellMods"

    old_version = "0.0.0+0"

    wrapper_main.write_var_to_makefile(
        wrapper_main.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        old_version,
        export=True,
    )

    rc, *_ = wrapper_dep.run_make(
        "cellinstall",
        module_version=old_version,
        cell_path=cell_path,
    )
    assert rc == 0

    rc, *_ = wrapper_main.run_make("cellinstall", module_version=old_version)
    assert rc == 0

    new_version = "1.0.0+0"

    rc, *_ = wrapper_dep.run_make(
        "cellinstall",
        module_version=new_version,
        cell_path=cell_path,
    )
    assert rc == 0

    wrapper_main.write_var_to_makefile(
        wrapper_main.config_module,
        f"{wrapper_dep.name}_DEP_VERSION",
        new_version,
        export=True,
    )

    rc, *_ = wrapper_main.run_make("cellinstall", module_version=new_version)
    assert rc == 0

    rc, outs, _ = run_ioc_get_output(
        module=wrapper_main.name,
        version=new_version,
        cell_path=wrapper_main.wrapper_dir / "cellMods",
    )
    assert rc == 0
    assert f"Loaded {wrapper_dep.name} version {new_version}" in outs


def test_that_setting_DEP_var_sets_dependency(wrapper_factory):
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()

    cell_path = wrapper_a.wrapper_dir / "cellMods"

    module_version = "0.0.0+0"

    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_b.name}_DEP_VERSION",
        module_version,
        export=True,
    )

    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0

    for dep_file in (cell_path / wrapper_a.name).rglob("*.dep"):
        contents = dep_file.read_text()

        assert len(contents) == 2
        assert contents[0].strip() == "# Generated file. Do not edit."
        assert f"{wrapper_b.name} {module_version}" == contents[1]


def test_make_clean_should_run_correctly_with_missing_dependencies(wrapper):
    wrapper.write_var_to_makefile(
        wrapper.config_module, "FOO_DEP_VERSION", "bar", export=True
    )

    rc, *_ = wrapper.run_make("clean")
    assert rc == 0


def test_recursive_db_dependency(wrapper_factory):
    wrapper_main = wrapper_factory.create()
    wrapper_dep = wrapper_factory.create()

    cell_path = wrapper_main.wrapper_dir / "cellMods"
    common_dir = wrapper_main.module_dir / f"O.{wrapper_main.base_version}_Common"

    wrapper_main.write_var_to_makefile(
        wrapper_main.module_makefile,
        f"{wrapper_dep.name}_DEP_VERSION",
        wrapper_dep.version,
        export=True,
    )
    substitution_file = wrapper_main.module_dir / "a.substitutions"
    substitution_file.write_text(
        """file "b.template"
{
    pattern { PATTERN }
            { SUBST }
}"""
    )
    wrapper_main.write_var_to_makefile(
        wrapper_main.module_makefile, "SUBS", "a.substitutions", modifier="+"
    )
    wrapper_main.write_var_to_makefile(
        wrapper_main.module_makefile,
        "USR_DBFLAGS",
        f"-I $(EPICS_MODULES)/{wrapper_dep.name}/$({wrapper_dep.name}_VERSION)/db",
        modifier="+",
    )

    template_file = wrapper_dep.module_dir / "b.template"
    template_file.write_text("record(ai, $(PATTERN)) {}")
    wrapper_dep.write_var_to_makefile(
        wrapper_dep.module_makefile, "TEMPLATES", "b.template", modifier="+"
    )

    rc, *_ = wrapper_dep.run_make("install", cell_path=cell_path)
    assert rc == 0

    rc, *_ = wrapper_main.run_make("cellbuild", cell_path=cell_path)
    assert rc == 0

    assert "SUBST" in (common_dir / "a.db").read_text()


@pytest.mark.skipif(sys.platform != "linux", reason="only works for linux")
def test_architecture_dependent_dependency(wrapper_factory):
    wrapper_a = wrapper_factory.create()
    wrapper_b = wrapper_factory.create()
    wrapper_c = wrapper_factory.create()

    cell_path = wrapper_a.wrapper_dir / "cellMods"

    module_version = "0.0.0+0"

    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_b.name}_DEP_VERSION_linux",
        module_version,
        export=True,
    )
    wrapper_a.write_var_to_makefile(
        wrapper_a.config_module,
        f"{wrapper_c.name}_DEP_VERSION_not_an_arch",
        module_version,
        export=True,
    )

    rc, *_ = wrapper_c.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_b.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0
    rc, *_ = wrapper_a.run_make(
        "cellinstall", module_version=module_version, cell_path=cell_path
    )
    assert rc == 0

    rc, outs, _ = run_ioc_get_output(
        module=wrapper_a.name,
        version=module_version,
        cell_path=wrapper_a.wrapper_dir / "cellMods",
    )
    assert rc == 0
    assert f"Loaded {wrapper_b.name} version {module_version}" in outs
    assert f"Loaded {wrapper_c.name} version {module_version}" not in outs
