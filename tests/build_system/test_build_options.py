import os
import re

import pytest


@pytest.mark.parametrize(
    "installed_archs, expected",
    [("foo", []), ("foo-bar foo-baz baz baz-qux", ["baz", "baz-qux"])],
)
def test_arch_filter(wrapper, installed_archs, expected):
    arch_regex = re.compile(r"Pass 2: T_A =\s*([^\s]+)")

    wrapper.write_var_to_makefile(
        wrapper.module_makefile,
        "CROSS_COMPILER_TARGET_ARCHS",
        installed_archs,
    )
    _, outs, _ = wrapper.run_make("debug", EXCLUDE_ARCHS="foo")

    host_arch = os.getenv("EPICS_HOST_ARCH")
    build_archs = [arch for arch in arch_regex.findall(outs) if arch != host_arch]
    assert build_archs == expected


def test_load_custom_config_files(wrapper):
    custom_config_file = wrapper.config_dir / "CONFIG_FOO"
    custom_config_file.write_text("$(error foobarbaz)")

    wrapper.add_file(custom_config_file)
    wrapper.write_var_to_makefile(
        wrapper.module_makefile, "CONFIGS", str(custom_config_file), modifier="+"
    )

    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert "foobarbaz" in errs
