def test_error_if_using_local_mode_with_loc_suffix(wrapper_factory):
    wrapper = wrapper_factory.create(E3_MODULE_SRC_PATH="test-loc")
    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert 'Local source mode "-loc" has been deprecated' in errs


def test_error_if_loading_from_outside_of_sitemods(wrapper):
    makefile_contents = wrapper.makefile.read_text()

    wrapper.makefile.write_text(
        makefile_contents.replace(
            "include $(REQUIRE_CONFIG)/RULES_SITEMODS",
            "include $(REQUIRE_CONFIG)/RULES_E3",
        )
    )

    rc, _, errs = wrapper.run_make("build")
    assert rc == 2
    assert "RULES_E3 should only be loaded from RULES_SITEMODS" in errs
