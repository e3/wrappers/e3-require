import sys


def test_make_clean_removes_library_files(wrapper):
    _ = wrapper.run_make("build clean")

    for file in wrapper.module_dir.rglob("*.*"):
        assert file.suffix != ".so" if sys.platform == "linux" else ".dylib"


def test_make_clean_removes_object_files(wrapper):
    _ = wrapper.run_make("build clean")

    for file in wrapper.module_dir.rglob("*.*"):
        assert file.suffix != ".o"
