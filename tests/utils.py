import subprocess
import time

RE_MISSING_FILE = "No rule to make target [`']{filename}'"
RE_MISSING_VERSION = "Module '{module}' version '{version}' does not exist."


def run_ioc_get_output(*args, **kwargs):
    """Run an IOC and try to load the test module."""
    ioc_args = []
    module = kwargs.get("module", None)
    version = kwargs.get("version", None)
    if module:
        ioc_args.append("-r")
        ioc_args.append(f"{module},{version}" if version else module)
    cell_path = kwargs.get("cell_path", None)
    if cell_path:
        # this is often/usually a Path object, so let's cast it for sanity of mind
        cell_path = str(cell_path)
        ioc_args.extend(["-l", cell_path])
    ioc_args.extend(args)

    proc = subprocess.Popen(
        ["iocsh", *ioc_args],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    time.sleep(1)
    try:
        stdout, stderr = proc.communicate(input=b"exit\n", timeout=5)
    except subprocess.TimeoutExpired:
        proc.kill()
        # Trying to run `subprocess.Popen.communicate()` can raise
        #
        #   ValueError: Invalid file object: <_io.BufferedReader name=7>
        #
        # when stdin is already closed.
        raise

    return proc.returncode, stdout.decode("utf-8"), stderr.decode("utf-8")
