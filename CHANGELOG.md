# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### New Features

* Remove `--realtime` option from `iocsh`
* Add timestamp for `LoadedModules`
* Deprecate `IOCNAME` environment variable in favour of `--iocname` argument for `iocsh`
* Add environment variable `E3_STRICT_CC`: if set to `YES` then driver.Makefile will
  fail if a cross-compiler is missing
* Support for e3 on Mac OS (both intel and apple silicon)

### Bugfixes

* Decouple `build` and `install` targets
* Exit `iocsh` if file to run does not exist
* Add trigger mapping on `LoadedModules` to silence QSRV2 warning
* Silence warnings on submodule initialisation
* Automatically use cell path with `test` target (no longer any need to activate an environment as part of the test rule)

## [5.1.1]

### Bugfixes

* Fix memory leaks on IOC exit
* Fix dirty terminal after IOC exit for some distributions
* Fix crash when running `make test` without having activated an environment
* Resolve `E3_CMD_TOP` when a startup script is being run with `iocsh` (previously expanded to just `.`)
* Fix truncated errorlog messages

## [5.1.0]

### New Features

* Allow for module-specific build rules (see: sequencer) to be installed and used
  within e3
* Automatically install LICENSE files with modules
* `afterInit` can now run commands of arbitrary length. Note: The syntax has changed from
  ```
  afterInit foo bar baz
  ```
  to
  ```
  afterInit 'foo bar baz'
  ```
* Add NTTable PV for module and version information as `LoadedModules`

### Bugfixes

* Fixed an issue where .template and .substitutions files with the same name would build incorrectly
* Fixed an issue where filenames with spaces in them that had been modified
in the wrapper would cause all of the module `make` commands to fail.
* Removed a number of memory leaks found by valgrind
* Fixed issue where failure of one of the patches to apply wouldn't stop the build
* Fixed memory leak in `afterInit`

### Other changes

* Rewrite `iocsh` converting it from being a shell script to a python (3.6) script
  * Remove support for file extensions: `.so`, `.dbd`, `.db`, `.substitutions`, `.template`, `.iocsh`
  * Remove support for `nice`
  * Remove support for sequencer programs
  * Remove optional argument passing to `gdb`
  * Change the IOC shell to use both stdout and stderr (previously only stdout)
  * Change default prompt
  * Change fallback IOC-name (used when `IOCNAME` is not set)
  * Multiple argument changes; e.g.
    * Shortened argument for printing version and exit changed from `-v` to `-V`
    * Make running IOC as realtime or with debuggers mutually exclusive
    * Change how arguments are passed to `gdb` and `valgrind` (see help: `--help`)
* Removed `tclx` script to expand .dbd in favour of `dbdExpand.pl` from EPICS base
* Removed `hdrs` target. Note that this has been entirely replaced by the automatic db expansion.
* Removed ability to pass `args` to require (which have not been used within e3)
* Removed `require module,ifexists` option
* Stop require from looking in legacy locations for various files
* Rewrite internal linked list to have better memory safety
* Removed enabling of core dump generation and max file size limit to allow developers more flexibility (enable and set limit by calling `ulimit -S -c x` where x is max size in blocks before IOC startup)
* Remove `ARCH_FILTER` support; from now on, only `EXCLUDE_ARCHS` is used.
* Remove `setE3Env.bash` alias (for `activate`)
* Remove custom `dbLoadTemplate` in favour of the one supplied by EPICS base
  * Note that this amounts to a change where double quotes (`"`) must be used in substitutions files when using macros in patterns

## [5.0.0]

### New Features
* Recursive dependency for headers at build-time. This is a more major change that involves:
  * Module dependecies are now fetched from `CONFIG_MODULE` in the sense that `X_DEP_VERSION` is parsed as a
    dependency on the module `x`
  * It is no longer necessary to specify `REQUIRED += ...` nor `x_VERSION = $(X_DEP_VERSION)` within the module makefile
* Check inconsistent versions between dependencies at build time. If an inconsistency is found the build will fail.

### Bugfixes
* Fixed issue where updated dependencies of substitution files did not retrigger a .db expansion.
* db expansion happens at build time, not at install time.

### Other changes
* Remove `IOCNAME:exit` PV. Stop loading `softIocExit.db` and now it's require that exposes the `BaseVersion` PV.
* Patch system now applies all patches in the `patch/` directory. Versioning for patches should be handled by git,
  not by require.
* The loop over `EPICSVERSION` in `driver.makefile` has been removed; various other cleanup has been performed.
* Improved output during IOC startup
* e3-sequencer tests added, as well as removing pre-processing of .stt files.
* Remove Win32 and CYGWIN32 support
* Remove `/tools` directory, including `test_installed_modules.sh` utility
* Remove `runScript`
* Drop file extension from `build_number.sh`
* Rename `setE3Env.bash` to `activate`
* Add alias from `setE3Env.bash` to `activate`
* Updated PV-names to be ESS compliant, and remove VERSIONS PV
* Changed from `build number` to `revision number`
* Added revision number parsing for test versions
* Joined together the variables `REQUIRE_CONFIG` and `E3_REQUIRE_CONFIG`
* Removed legacy file `DECOUPLE_FLAGS`

## [4.0.0]

### New Features
* Block module loading after iocInit has been invoked.
* Arguments have been added to `iocsh.bash` to enable user to pass any debugger options to GDB and Valgrind.
* Autocompletion for `iocsh.bash` has been added
* Removed `iocsh_gdb.bash` and `iocsh_nice.bash`, both of whose functionality can be called via `iocsh.bash -dg` and `iocsh.bash -n`, respectively.
* Patch file location has been change from `patch/Site/$VERSION-description.p0.patch` to `patch/Site/$VERSION/description.p0.patch`
* Require will automatically build `.template` and `.substitutions` files into the common build directory instead of into the source Db path
* Rudimentary testing has been added:
* * Tests that the correct version is loaded
* * Tests that elementary patching/building works as expected
* Added consistency check between e3 environment variables and path to `iocsh.bash`. `iocsh.bash` will abort if these are not consistent.
* Add e3 version infomation to the shell prompt
* Add option to allow override of automatic addition of `iocInit` to generated startup script

### Bugfixes
* `iocsh.bash --help` (and variants) no longer loads tries to load `env.sh`.
* `make uninstall` no longer tries to remove from global `siteLibs` directory.
* `make build` will fail if any of the target architectures fail, not just the last one.
* Fixed issue where git passwords would be displayed in plaintext in metadata file
* Fixed issue where `LD_LIBRARY_PATH` could keep old `siteLibs` path around
* Removed duplicated entries from generated `.dep` files
* Removed `dev` targets from modules that function in "local source mode"
* Fixed issue where `.hpp` files were not installed correctly with `KEEP_HEADER_SUBDIRS`
* Missing `REQUIRED` dependencies now cause the build to fail instead of providing a warning
* Fixed issue where consecutive builds might not track updated dependencies
* Fixed an issue related to buffering of data being written to a shared filesystem which produced garbled `.dep` files

### Other changes
* Rename `iocsh.bash` to `iocsh`
* Removed `<module>_TEMPLATES` in favour of `<module>_DB`
* Removed unnecessary code from `make init`.
* removed `e3.cfg`, `ess-env.conf` and `DEFINES_REQUIRE` files and associated codes in `RULES_REQUIRE`, `setE3Env.bash` and `.gitignore`.
* removed legacy code from setE3Env.bash
* Removed usage of `env.sh` - now there is a check only for seeing if the environment variable `$IOCNAME` is set
* Fix typos in `iocsh_functions.bash` comments
* Rearrange usage to match order of options in code
* Add information about realtime option to usage
* Cleaned up the output of `make vars` and `make devvars`
* Merged together `configure/E3` and `configure/MODULES`
* Removed `siteLibs` and `siteApps` directories
* Removed references to EPICS Base v3

## [3.4.1]

### Bugfixes
* Fixed an issue where `iocshRegisterCommon()` was called when registering functions for modules as they are loaded. This had
  the effect of overwriting any functions that have the same name as a common one with their original one (e.g. `dbLoadTemplate`
  from `require`)
* Fixed the shebang from `build_number.sh` so that it works in Ubuntu

## [3.4.0]
3.4.0 is also a fairly major release, that fixes many bugs but that also enables use of cellmode, to facilitate work against a shared build.

### New Features
* Added back `promptE3Env.bash`, which runs `setE3Env.bash` and then sets the bash prompt.
* Added descriptions for more targets when running `make help`
* Added a new test target for individual modules, available as `make test`
* Added script in `tools/test_installed_modules.sh` that tests all modules in an installation
* Build numbers are now prefixed with a `+`.
* Cell mode: When using `make cellinstall`, the module is installed in the path
  `${E3_CELL_PATH}/${EPICS_BASE_VERSION}/${E3_REQUIRE_VERSION}/module_name`. This allows
  us to distinguish which version of base and require were used for the build.
* A new make target, `cellbuild` has been added. This simply allows you to build (not install)
  while still possibly linking with other Cell mode modules.

### Bugfixes
* Removed `loadIocsh` function, which was just a cover for `runScript`.
* Removed references to `INSTBASE`
* Fixed issue where `make cellinstall` would recompile a module.
* Fixed permissions on `cellinstall`.
* Fixed issue where `make debug` would recompile a module.
* Fixed issue where `make install` would fail if you had not run `make build` first.
* Removed all references to `sudo`. If `sudo` needs to be used, it should be done manually.
* Fixed issue with module name checking which caused modules to fail to build on certain linux distributions
* Exact module version is fetched correctly at build-time to ensure that new releases do not affect upstream dependencies
* Issue involving priority between some test versions fixed
* `make patch` when there are no patch files will no longer return an error code.

### Other changes
* Removed `require` as a submodule and merged it into `e3-require`.
* Add `CONFIG_SHELL` to define standard shell (*bash*) used by *Make*
* Removed `plotdep` target.

## [3.3.0]
3.3.0 is a relatively major release with many bugfixes and a number of features added based on consultation
with developers. One major change is that versions are now parsed as MAJOR.MINOR.PATCH-BUILD (with build
number being optional, assumed to be zero if not present). This has proven to be necessary in the NFS
setting for a number of separate reasons to handle dependencies properly. Note the following behavior:

* `require module` - loads the latest numeric version of a given module.
* `require module,x.x.x` - loads the specific numeric version of a module, with the highest build number
* `require module,x.x.x-x` - loads a specific build number of the specified version of a module

A second major change (mostly via bugfixes) is that the local install command, `make cellinstall` which installs the module in a local directory now works properly, which allows developers and integrators to simply mount the NFS E3 build and work with that instead of needing to install E3 locally.

### New Features
* Removed all EPICS 3.* and VxWorks code, as these are not to be supported at ESS.
* Consistent with the philosophy of not requiring module version pinning, if one specifies a dependent
  module with e.g. `REQUIRED += asyn` then the latest version of asyn will be used. No version need
  to be specified.
* Installed modules will be installed together with a metadata file containing information about the
  module and wrapper, including the git status of the wrapper/module, the latest tag/commit hash, and
  the git url for the wrapper.
* Build numbers are now supported properly
* `iocsh.bash` will load `env.sh` at startup (which can be specified), which allows `IOCNAME` to be
  specified at startup
* Added the ability to install header files while preserving directory structure instead of flattening
  all header files into a single module/version/includes directory.
* Using `EPICS_MODULE_TAG` to detect if we are using the local source code vs. git submodule. Note that
  the old -loc is still supported, but will be deprecated in a future release.
* Added a `prebuild` target that runs before build so module developers can run specific code before the build process.
* A module developer can now install dbd files separate from the module dbd file by using `DBD_INSTALLS += file.dbd`.

### Bugfixes
* Ensures that lowercase module names are enforced consistently
* Vendor libraries are only installed at install time, not at build time
* Vendor libraries are uninstalled when `make uninstall` is run
* `make debug` can now be run before running `make build`
* Patches that involve renaming files can be used
* When using the "cellinstall" mode to install modules for local testing, sudo has been removed.
* When using "cellinstall", sequencer is loaded correctly
* When using "cellinstall", module include files are located correctly
* `iocsh.bash` now supports multiple directories being specified with the -l (local) flag as a source of loading modules
* Fixed an error where mounting the NFS E3 in a location other than `/epics` would result in failed builds
* Assorted minor typos in scripts
* Re-added functionality to have `iocsh.bash` automagically source `setE3Env.bash`.

## [3.2.0]
Require 3.2.0 is the first version of require used during our initial return to the NFS implementation
of E3. It maintains some of the work done there (modules are lowercased for consistency), but mostly
reverts back to a similar version to require 3.1.2. One major difference is that module pinning is not
"required" (pun only semi-intended). That is, a startup script can simply include a statement of the
form `require module` instead of `require module,version` as was the case in 3.1.2. An IOC developer can
of course pin a specific module version. If they do not, then the highest numeric version will be chosen.

### New Features
* Added -dg, -dv options to run gdb and valgrind using `iocsh.bash`
* If `IOCNAME` is defined, then it is used in the PV names set by require instead of `REQMOD:$(hostname)-$(pid)`.
* Reverted changes from conda require to return to NFS
* Moved tools files from e3-require to require submodule

### Bugfixes
* Fixed issue where a second user running `iocsh.bash` on a machine would be unable to create the temporary
  startup script


[Unreleased]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/5.1.1...master
[5.1.1]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/5.1.0...5.1.1
[5.1.0]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/5.0.0...5.1.0
[5.0.0]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/4.0.0...5.0.0
[4.0.0]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/3.4.1...4.0.0
[3.4.1]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/3.4.0...3.4.1
[3.4.0]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/3.3.0...3.4.0
[3.3.0]: https://gitlab.esss.lu.se/e3/e3-require/-/compare/3.2.0...3.3.0
[3.2.0]: https://gitlab.esss.lu.se/e3/e3-require/-/tree/3.2.0
