#!/usr/bin/env bash

_iocsh_completion() {
  local cur prev opts mods
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD - 1]}"

  #  -l should only autocomplete to directories
  if [[ ${prev} == -l ]]; then
    mapfile -t COMPREPLY < <(compgen -d -- "${cur}")
    return 0
  fi

  #  -e should only autcomplete to filenames
  if [[ ${prev} == -e ]]; then
    mapfile -t COMPREPLY < <(compgen -f -- "${cur}")
    return 0
  fi

  #  -r should try to determine modules
  # Note that this does not return anything in cellMods, which would require us to
  # determine what cellpath to use.
  if [[ ${prev} == -r ]]; then
    if [ -n "${EPICS_BASE}" ] && [ -n "${E3_REQUIRE_VERSION}" ]; then
      mods=$(ls "${EPICS_BASE}/require/${E3_REQUIRE_VERSION}/siteMods/" 2>/dev/null)
      mapfile -t COMPREPLY < <(compgen -W "${mods}" -- "${cur}")
    fi
    return 0
  fi

  opts="-h -v -c -s -r -e -dg -dv -l -n"
  if [[ ${cur} == -* ]]; then
    mapfile -t COMPREPLY < <(compgen -W "${opts}" -- "${cur}")
    return 0
  fi

  mapfile -t COMPREPLY < <(compgen -f -- "${cur}")
}

complete -o filenames -o nospace -o bashdefault -F _iocsh_completion iocsh
