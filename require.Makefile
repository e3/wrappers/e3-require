#  Copyright (c) 2004 - 2017     Paul Scherrer Institute
#  Copyright (c) 2017 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#  Author:      Dirk Zimoch (PSI)
#  Maintainer:  Simon Rose (ESS) <simon.rose@ess.eu>


# This is the one time path in order to compile and install
# require within EPICS Environment. After the installation
# include $(E3_REQUIRE_TOOLS)/driver.makefile should be used
#
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# It is easy to maintain RULES_E3 if we use the "repository" makefile
# instead of the installed makefile.
#
include $(where_am_I)/require-ess/tools/driver.makefile

APP := .
APPSRC := $(APP)/src
APPDB := $(APP)/Db

SOURCES += $(APPSRC)/require.c
SOURCES += $(APPSRC)/version.c
SOURCES += $(APPSRC)/afterInit.c
SOURCES += $(APPSRC)/common.c
SOURCES += $(APPSRC)/module.c
DBDS    += $(APPSRC)/require.dbd
DBDS    += $(APPSRC)/afterInit.dbd

HEADERS += $(APPSRC)/require.h

# HEADERS += require_env.h

# We need to find the Linux link.h before the EPICS link.h
#
USR_INCLUDES_Linux=-idirafter $(EPICS_BASE)/include

USR_CFLAGS += -std=gnu99

# ESS require doesn't use T_A, because Linux should handle linux as "1"
# instead of its name. ESS require can handle them within the EPICS
# IOC shell internally.
#
# USR_CFLAGS += -DT_A='"${T_A}"'

TEMPLATES += $(APPDB)/moduleversion.template
