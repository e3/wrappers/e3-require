# e3-require

The e3 version of `require` based on the PSI version of [`require`](https://github.com/paulscherrerinstitute/require).
There are significant diferences between the two modules, significant enough that it is not proper to call it a fork
any longer.

At its heart, `require` performs:
* Dynamic loading of EPICS modules and rudimentary dependency resolution
* Loading of IOCs using a custom startup script calling `softIocPVA`
* Building of EPICS modules into standardized e3 packages

For more documentation on this, please see [e3.pages](http://e3.pages.esss.lu.se/design/2_require.html).

## Structural differences between PSI require and ESS require

The main differences between the two is that ESS `require` includes a number of configuration files defining special
build targets for EPICS modules. For example, the following targets are defined.
* `make build` - Runs the custom build process defined in `require`.
* `make vars` - Displays a number of custom build variables e.g. install locations, etc.
* `make debug` - Runs a debug build process, displaying information about the current build
* `make cellinstall` - Installs the module in a local directory, e.g. for testing purposes

For more special rules, look at the files in [configure/modules].

The main parts that we have taken from PSI `require` are `require.c` and `driver.makefile`, which respectively
load modules dynamically at run-time, and which run the custom build process. However, both of these have been
modified significantly for the purposes of e3.

Furthermore, we have added the custom startup script `iocsh` which sets up the IOC's environment before
running `softIocPVA` from EPICS base.

## Building `require`

In order to build require, you must first have EPICS base installed. After that, you must configure `CONFIG_MODULE`
and `RELEASE` to specify the require version and the location of EPICS base. Having done so, run
```bash
$ make build
$ make install
```

To test your build, run
```bash
$ source ${EPICS_BASE}/require/${E3_REQUIRE_VERSION}/bin/activate
$ iocsh
```

which should start up an IOC and load require.
